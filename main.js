var Xvfb = require('xvfb');
var Nightmare = require('nightmare');
var request = require('request');
var fs = require('fs');

var config = JSON.parse(fs.readFileSync('config.json', 'utf8'));
var xvfb = new Xvfb({
    silent: true
});
xvfb.startSync();

var nightmare = Nightmare({
    show: false,
});
nightmare
    .useragent('Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36')
    .viewport(1886, 1089)
    .goto('http://www.cinemasgaumontpathe.com/cinemas/' + config.cinema)
    .evaluate(function() {
        var films = {};

        return Array.prototype.slice.call(document.querySelectorAll('.filmv2')).filter((e) => {
            return !!e.querySelector('.titre_choix h6');
        }).filter((e) => {
            var titre = e.querySelector('.titre_choix h6').innerText;
            if(titre in films)
                return false;
            else {
                films[titre] = true;
                return true;
            }
        }).map((e) => {
            var affiche = e.querySelector('.affiche').querySelector('img').src;
            if(affiche.indexOf('http://www.cinemasgaumontpathe.com/') !== 0)
                affiche = 'http://www.cinemasgaumontpathe.com/' + affiche;

            var seances = {};
            Array.prototype.slice.call(e.querySelectorAll('.cin-list > ul > li')).map(function(day) {
                var date = day.getAttribute('class').split(' ')[1].split('_')[2];
                seances[date] = Array.prototype.slice.call(day.querySelectorAll('.sceance-reserver')).map((seance) => {
                    return {
                        type: seance.getAttribute('class').split(' ')[1].split('_')[1],
                        heures: Array.prototype.slice.call(seance.querySelectorAll('a.bulle-horaire')).map((horaire) => {
                            return {
                                href: horaire.href,
                                heure: horaire.innerText.trim(),
                            };
                        })
                    };
                });
            });

            return {
                libelle: e.querySelector('.libelle') ? e.querySelector('.libelle').innerText : null,
                affiche: affiche,
                url: e.querySelector('.btn-red2 a').href,
                titre: e.querySelector('.titre_choix h6').innerText,
                genre: e.querySelector('.time a') ? e.querySelector('.time a').innerText : null,
                duree: e.querySelector('.time') ? e.querySelector('.time').innerText.substr(e.querySelector('.time').innerText.indexOf('Durée : ') + 8).trim() : null,
                seances: seances,
            };
        });
    })
    .end()
    .then((films) => {
        var old = JSON.parse(fs.readFileSync(config.cinema + '.json', 'utf8'));

        var chain = Promise.resolve();

        // find new films
        films.forEach((newFilm) => {
            var isNew = old.find((oldFilm) => {
                return oldFilm.titre === newFilm.titre;
            }) === undefined;

            if(isNew) {
                var text = '[-](' + newFilm.affiche + ') [' + newFilm.titre + '](' + newFilm.url + ') (' + newFilm.duree + ')  \r\n' + newFilm.genre + (newFilm.libelle ? (' - ' + newFilm.libelle) : '') + '  \r\n';
                for(var date in newFilm.seances) {
                    newFilm.seances[date].forEach((g) => {
                        text += date + ' ' + g.type + ': ' + g.heures.map((heure) => {
                            return '[' + heure.heure + '](' + heure.href + ')';
                        }).join(', ') + '  \r\n';
                    });
                }

                chain = chain.then(() => {
                    console.log(newFilm.titre);
                    return new Promise((resolve, reject) => {
                        request({
                            uri: 'https://api.telegram.org/bot' + config.telegram.token + '/sendMessage',
                            method: 'POST',
                            json: {
                                chat_id: config.telegram.chat,
                                text: text,
                                parse_mode: 'Markdown',
                            }
                        }, function (error, response, body) {
                            if(error) {
                                console.log(body);
                                reject();
                            }
                            setTimeout(() => {
                                resolve();
                            }, 1000);
                        });
                    });
                });
            }
        });
        fs.writeFileSync(config.cinema + '.json', JSON.stringify(films));

        chain.then(() => {
            xvfb.stopSync();
        });
    })
    .catch(function (error) {
        console.error('Search failed:', error);
        xvfb.stopSync();
    });
